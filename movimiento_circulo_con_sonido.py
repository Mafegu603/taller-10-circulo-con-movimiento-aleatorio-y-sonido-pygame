import pygame
import random

pygame.init()
pygame.mixer.init()
sheight=500
swidth=500
screen= pygame.display.set_mode((swidth,sheight))
myclock=pygame.time.Clock()

ball_color=(255,0,0)
bg_color=(255,255,255)
x=swidth/2
y=sheight/2
ball_size=30
mv_x='izquierda'
mv_y='arriba'
alat=random.randint(1,50)

sound= pygame.mixer.Sound('PELOTA.wav')

while True:
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            pygame.quit()
    screen.fill(bg_color)
    pygame.draw.circle(screen,ball_color,(x,y),ball_size)
    if x==500:
        mv_x='izquierda'
        sound.play()
    if x==0:
        mv_x='derecha'
        sound.play()
    if mv_x=='izquierda':
        x-=1
        sound.play()
    else:
        x+=1
        sound.play()
        
    if y>=500:
        mv_y='arriba'
        sound.play()
    if y<=0:
        mv_y='abajo'
        sound.play()
    
    if mv_y=='arriba':
        y-=alat
        sound.play()
    else:
        y+=alat
        sound.play()
    pygame.display.update()
    myclock.tick(30)
    